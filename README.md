Ubuntu 14.04 LTS Server (and Desktop) VirtualBox Vagrant
========================================================

Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](http://www.vagrantup.com/downloads.html).

Native installers are available for OSX, Windows and Linux.

At the moment of the writing the versions were:

* VirtualBox 4.3.20
* Vagrant 1.6.3

Grap a copy of [Ubuntu 14.04 LTS server iso image](http://releases.ubuntu.com/14.04/).

Ready-made Base Boxes
---------------------

The boxes are available in [HashiCorp Atlas](https://atlas.hashicorp.com/learn/vagrant):

* [Ubuntu 14.04 LTS Desktop](https://atlas.hashicorp.com/janihur/boxes/ubuntu-1404-desktop)
* [Ubuntu 14.04 LTS Server](https://atlas.hashicorp.com/janihur/boxes/ubuntu-1404-server)

Installing Vagrant compatible Ubuntu Server
-------------------------------------------

### VirtualBox settings

* Base memory: 256MB
* Hard drive: VDI, dynamically allocated, 100GB
* Boot order: remove floppy
* Audio: disable
* USB controller: disable
* Network: paravirtualized (?)

### Ubuntu installation

* In the beginning press F4 and select: Install a minimal virtual machine
* username/password: vagrant/vagrant
* Partition disks: use entire disk
* No automatic updates
* Software selection: OpenSSH server
* Reboot at the end of the installation
* Login with ssh and configure the system

### Password-less sudo for vagrant user

    $ cat /etc/sudoers.d/vagrant
    vagrant ALL=(ALL) NOPASSWD: ALL

### Update packages

    sudo apt-get update
    sudo apt-get -y upgrade
    sudo apt-get clean

### Vagrant insecure keypair

    mkdir -m 0700 ~/.ssh
    wget --no-check-certificate \
      https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub \
      -O ~/.ssh/authorized_keys
    chmod 0600 .ssh/authorized_keys

### VirtualBox additions

    sudo apt-get -y install dkms build-essential
    sudo apt-get clean
    # mount additions iso-image with VB GUI
    sudo mount /dev/cdrom /media/cdrom/
    sudo sh /media/cdrom/VBoxLinuxAdditions.run

### Compact disk

On guest:

    sudo dd if=/dev/zero of=/EMPTY bs=1M; sudo rm -f /EMPTY
    sudo shutdown -P now

On host:

    VBoxManage modifyhd /path/to/ubuntu-server-1404/ubuntu-server-1404.vdi --compact

Compacting is only supported by the VDI-file format. Size reduction was from 1555MB to 1074MB.

The workaround for non-VDI formats is to [clone the disk](https://www.virtualbox.org/manual/ch08.html#vboxmanage-clonevdi) after it has been zeroed out:

    VBoxManage clonehd ec9c2a4a-cdae-4e86-b671-6a7e914e0359 box-disk2.vmdk

Hard disk uuid can be found with:

    VBoxManage showhdinfo /path/to/box-disk2.vmdk

And then attach the cloned disk to virtual machine:

    VBoxManage storageattach <VMNAME> \
                         --storagectl "SATA" \
                         --device 0 \
                         --port 0 \
                         --type hdd \
                         --medium box-disk2.vmdk

### Package Vagrant box

    vagrant package --base ubuntu-1404-server --output ubuntu-1404-server.box

Note the [exported virtual disk image format](https://www.virtualbox.org/manual/ch08.html#vboxmanage-export) will be VMDK.

Update Ubuntu Server
--------------------

### Update packages

    sudo apt-get update
    sudo apt-get -y upgrade
    sudo apt-get -y dist-upgrade
    sudo apt-get clean
    sudo reboot

### Remove old kernels

    # current kernel - don't remove
    uname -r
    # all installed kernels
    dpkg --list | grep linux-image
    # remove old kernels
    sudo apt-get purge linux-image-x.x.x-x-generic
    sudo reboot

### Update VirtualBox additions

    sudo mount /dev/cdrom /media/cdrom/
    sudo sh /media/cdrom/VBoxLinuxAdditions.run
    sudo reboot

Installing Vagrant compatible Ubuntu Desktop
--------------------------------------------

Clone the previously create virtual machine with VirtualBox GUI. Make sure to tick _Reinitialize
the MAC addresses of all network cards_.

clone type: full clone

### VirtualBox settings

Base memory: 1024 MB

### Update Packages

    sudo apt-get update
    sudo apt-get -y upgrade
    sudo apt-get clean

### Desktop

You can select other desktops too. I prefer [lubuntu](http://lubuntu.net/).

    sudo apt-get install lubuntu-desktop
    sudo apt-get clean
    # reboot
    sudo shutdown -r now

### VirtualBox additions

    # mount additions iso-image with VB GUI
    sudo mount /dev/cdrom /media/cdrom/
    sudo sh /media/cdrom/VBoxLinuxAdditions.run

### Set hostname

    sudo vi /etc/hostname
    sudo vi /etc/hosts
    sudo hostname <HOSTNAME>

### Compact disk

See above. Size reduction was from 3.8GB to 2.9GB.

### Package Vagrant box

See above.

### Vagrant configuration

Configure Vagrant to show the desktop before `vagrant up`:

    config.vm.provider "virtualbox" do |vb|
      vb.gui = true
    end

Update Ubuntu Desktop
---------------------

See Update Ubuntu Server.
